#include "Life.hpp"
#include "ConwayCell.hpp"
#include "FredkinCell.hpp"
#include "Cell.hpp"

template <typename T>
Life<T>::Life(int rows, int cols) : grid(rows, vector<T>(cols, T(false)))
{
    this->rows = rows;
    this->cols = cols;
    this->pop = 0; // start with no live cells
}

template <typename T>
void Life<T>::addLiveCell(int row, int col)
{
    if (!grid[row][col].isAlive())
        pop += 1;
    grid[row][col] = T(true);
}

template <typename T>
void Life<T>::turn()
{
    //get grid containing int counts of neighbors
    vector<vector<int>> neighbors(rows, vector<int>(cols, 0));
    calculateNeighbors(neighbors);
    //update the cells for the next generation
    updateCells(neighbors);
}

template <typename T>
void Life<T>::calculateNeighbors(vector<vector<int>> &neighbors)
{
    for (int r = 0; r < rows; r++)
    {
        for (int c = 0; c < cols; c++)
        {
            if (grid[r][c].isAlive())
            {
                if (r > 0 && grid[r - 1][c].checkIfNeighbor(r - 1, c, r, c))
                    ++neighbors[r - 1][c];
                if (c > 0 && grid[r][c - 1].checkIfNeighbor(r, c - 1, r, c))
                    ++neighbors[r][c - 1];
                if (r < rows - 1 && grid[r + 1][c].checkIfNeighbor(r + 1, c, r, c))
                    ++neighbors[r + 1][c];
                if (c < cols - 1 && grid[r][c + 1].checkIfNeighbor(r, c + 1, r, c))
                    ++neighbors[r][c + 1];
                if (r > 0 && c > 0 && grid[r - 1][c - 1].checkIfNeighbor(r - 1, c - 1, r, c))
                    ++neighbors[r - 1][c - 1];
                if (r < rows - 1 && c > 0 && grid[r + 1][c - 1].checkIfNeighbor(r + 1, c - 1, r, c))
                    ++neighbors[r + 1][c - 1];
                if (r > 0 && c < cols - 1 && grid[r - 1][c + 1].checkIfNeighbor(r - 1, c + 1, r, c))
                    ++neighbors[r - 1][c + 1];
                if (r < rows - 1 && c < cols - 1 && grid[r + 1][c + 1].checkIfNeighbor(r + 1, c + 1, r, c))
                    ++neighbors[r + 1][c + 1];
            }
        }
    }
}

template <typename T>
void Life<T>::updateCells(vector<vector<int>> &neighbors)
{
    pop = 0;
    for (int r = 0; r < rows; r++)
    {
        for (int c = 0; c < cols; c++)
        {
            pop += grid[r][c].updateCell(neighbors[r][c]) ? 1 : 0;
        }
    }
}

template <typename T>
void Life<T>::printGrid(ostream &sout, int generation)
{
    sout << "Generation = " << generation << ", Population = " << pop << "." << endl;
    for (int r = 0; r < rows; r++)
    {
        for (int c = 0; c < cols; c++)
        {
            sout << grid[r][c].charRep();
        }
        sout << endl;
    }
}

template <typename T>
Life<T>::~Life()
{
    grid.clear();
}

template class Life<ConwayCell>;
template class Life<FredkinCell>;
template class Life<Cell>;
