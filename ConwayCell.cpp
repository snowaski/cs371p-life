#include "ConwayCell.hpp"

ConwayCell::ConwayCell(bool alive) : AbstractCell(alive) {}

ConwayCell::ConwayCell() : AbstractCell(false) {}

ConwayCell::ConwayCell(const ConwayCell &) : AbstractCell(false) {}

char ConwayCell::charRep()
{
    if (_alive)
        return '*';
    else
        return '.';
}

bool ConwayCell::updateCell(int neighbors)
{
    // cout << "ConwayCell::updateCell" << endl;
    // bring to life if 3 live neighbors
    if (!_alive && neighbors == 3)
        _alive = true;
    // kill if less than 2 live neighbors or more than 3
    if (_alive && (neighbors < 2 || neighbors > 3))
        _alive = false;
    //use to track pop
    return _alive;
}

ConwayCell *ConwayCell::clone() const
{
    return new ConwayCell(*this);
}

bool ConwayCell::checkIfNeighbor(int cRow, int cCol, int nRow, int nCol)
{
    return true;
}