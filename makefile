.DEFAULT_GOAL := all
MAKEFLAGS     += --no-builtin-rules
SHELL         := bash

ASTYLE        := astyle
CHECKTESTDATA := checktestdata
CPPCHECK      := cppcheck
DOXYGEN       := doxygen
VALGRIND      := valgrind

ifeq ($(shell uname -s), Life)
    BOOST    := /usr/local/include/boost
    CXX      := g++-10
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -I/usr/local/include -Wall -Wextra
    GCOV     := gcov-10
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -lgtest -lgtest_main
    LIB      := /usr/local/lib
else ifeq ($(shell uname -p), unknown)
    BOOST    := /usr/include/boost
    CXX      := g++
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    GCOV     := gcov
    GTEST    := /usr/include/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
    LIB      := /usr/lib
else
    BOOST    := /usr/include/boost
    CXX      := g++-9
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    GCOV     := gcov-9
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
    LIB      := /usr/local/lib
endif

# run docker
docker:
	docker run -it -v $(PWD):/usr/gcc -w /usr/gcc gpdowning/gcc

# get git config
config:
	git config -l

# get git log
Life.log:
	git log > Life.log

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status

# download files from the Life code repo
pull:
	make clean
	@echo
	git pull
	git status

# upload files to the Life code repo
push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Life.cpp
	git add Life.hpp
	-git add Life.log
	-git add html
	git add makefile
	git add README.md
	git add RunLife.cpp
	git add RunLife.ctd
	git add RunLife.in
	git add RunLife.out
	git add TestLife.cpp
	git commit -m "another commit"
	git push
	git status

# compile run harness
RunLifeConway: Life.hpp Life.cpp AbstractCell.hpp AbstractCell.cpp Cell.hpp Cell.cpp ConwayCell.hpp ConwayCell.cpp FredkinCell.hpp FredkinCell.cpp RunLifeConway.cpp
	-$(CPPCHECK) RunLifeConway.cpp
	-$(CPPCHECK) Life.cpp
	-$(CPPCHECK) ConwayCell.cpp
	-$(CPPCHECK) AbstractCell.cpp
	-$(CPPCHECK) FredkinCell.cpp
	-$(CPPCHECK) Cell.cpp
	$(CXX) $(CXXFLAGS) Life.cpp Cell.cpp ConwayCell.cpp FredkinCell.cpp AbstractCell.cpp RunLifeConway.cpp -o RunLifeConway

# compile run harness
RunLifeFredkin: Life.hpp Cell.hpp Cell.cpp Life.cpp AbstractCell.hpp AbstractCell.cpp ConwayCell.hpp ConwayCell.cpp FredkinCell.hpp FredkinCell.cpp RunLifeFredkin.cpp
	-$(CPPCHECK) RunLifeFredkin.cpp
	-$(CPPCHECK) Life.cpp
	-$(CPPCHECK) FredkinCell.cpp
	-$(CPPCHECK) AbstractCell.cpp
	-$(CPPCHECK) ConwayCell.cpp
	-$(CPPCHECK) Cell.cpp
	$(CXX) $(CXXFLAGS) Life.cpp Cell.cpp FredkinCell.cpp ConwayCell.cpp AbstractCell.cpp RunLifeFredkin.cpp -o RunLifeFredkin

# compile run harness
RunLifeCell: Life.hpp Life.cpp AbstractCell.hpp AbstractCell.cpp Cell.hpp Cell.cpp ConwayCell.hpp ConwayCell.cpp FredkinCell.hpp FredkinCell.cpp RunLifeCell.cpp
	-$(CPPCHECK) RunLifeCell.cpp
	-$(CPPCHECK) Life.cpp
	-$(CPPCHECK) FredkinCell.cpp
	-$(CPPCHECK) AbstractCell.cpp
	-$(CPPCHECK) ConwayCell.cpp
	$(CXX) $(CXXFLAGS) Life.cpp Cell.cpp AbstractCell.cpp FredkinCell.cpp ConwayCell.cpp RunLifeCell.cpp -o RunLifeCell

# compile test harness
TestLife: Life.hpp Life.cpp TestLife.cpp AbstractCell.hpp AbstractCell.cpp ConwayCell.hpp ConwayCell.cpp FredkinCell.hpp FredkinCell.cpp Cell.hpp Cell.cpp
	-$(CPPCHECK) RunLifeConway.cpp
	-$(CPPCHECK) Life.cpp
	-$(CPPCHECK) ConwayCell.cpp
	-$(CPPCHECK) AbstractCell.cpp
	-$(CPPCHECK) FredkinCell.cpp
	$(CXX) $(CXXFLAGS) Life.cpp Cell.hpp Cell.cpp ConwayCell.cpp FredkinCell.cpp AbstractCell.cpp TestLife.cpp -o TestLife $(LDFLAGS)

# run/test files, compile with make all
FILES :=        \
    RunLifeConway  \
	RunLifeFredkin \
	RunLifeCell    \
    TestLife      

# compile all
all: $(FILES)

# check integrity of input file
ctd-check:
	$(CHECKTESTDATA) RunLife.ctd RunLife.in

# generate a random input file
ctd-generate:
	$(CHECKTESTDATA) -g RunLife.ctd RunLifeCellIN.tmp
	$(CHECKTESTDATA) -g RunLife.ctd RunLifeConwayIN.tmp
	$(CHECKTESTDATA) -g RunLife.ctd RunLifeFredkinIN.tmp

# execute run harness and diff with expected output
run-conway: RunLifeConway
	./RunLifeConway < RunLifeConway.in > RunLifeConway.tmp
	-diff RunLifeConway.tmp RunLifeConway.out

# execute run harness and diff with expected output
run-fredkin: RunLifeFredkin
	./RunLifeFredkin < RunLifeFredkin.in > RunLifeFredkin.tmp
	-diff RunLifeFredkin.tmp RunLifeFredkin.out

# execute run harness and diff with expected output
run-cell: RunLifeCell
	./RunLifeCell < RunLifeCell.in > RunLifeCell.tmp
	-diff RunLifeCell.tmp RunLifeCell.out

# execute test harness
test: TestLife
	$(VALGRIND) ./TestLife
	$(GCOV) -b Life.cpp | grep -B 2 "cpp.gcov"

# clone the Life test repo
life-tests:
	git clone https://gitlab.com/gpdowning/cs371p-life-tests.git life-tests

# test files in the Life test repo
TFILES := `ls life-tests/*-RunLifeCell.in`

# execute run harness against a test in Life test repo and diff with expected output
life-tests/%: RunLifeCell
	./RunLifeCell < $@.in > RunLifeCell.tmp
	-diff RunLifeCell.tmp $@.out

# execute run harness against all tests in Life test repo and diff with expected output
tests: life-tests RunLifeCell
	-for v in $(TFILES); do make $${v/.in/}; done

# auto format the code
format:
	$(ASTYLE) Life.cpp
	$(ASTYLE) Life.hpp
	$(ASTYLE) RunLife.cpp
	$(ASTYLE) TestLife.cpp

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATIC  to YES
# create Doxfile
Doxyfile:
	$(DOXYGEN) -g

# create html directory
html: Doxyfile Life.hpp
	$(DOXYGEN) Doxyfile

# check files, check their existence with make check
CFILES :=          \
    .gitignore     \
    .gitlab-ci.yml \
    Life.log    \
    html

# check the existence of check files
check: $(CFILES)

# remove executables and temporary files
clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f RunLife
	rm -f TestLife

# remove executables, temporary files, and generated files
scrub:
	make clean
	rm -f  Life.log
	rm -f  Doxyfile
	rm -rf life-tests
	rm -rf html
	rm -rf latex

# output versions of all tools
versions:
	@echo "% shell uname -p"
	@echo  $(shell uname -p)
	@echo
	@echo "% shell uname -s"
	@echo  $(shell uname -s)
	@echo
	@echo "% which $(ASTYLE)"
	@which $(ASTYLE)
	@echo
	@echo "% $(ASTYLE) --version"
	@$(ASTYLE) --version
	@echo
	@echo "% which $(CHECKTESTDATA)"
	@which $(CHECKTESTDATA)
	@echo
	@echo "% $(CHECKTESTDATA) --version"
	@$(CHECKTESTDATA) --version
	@echo
	@echo "% which cmake"
	@which cmake
	@echo
	@echo "% cmake --version"
	@cmake --version
	@echo
	@echo "% which $(CPPCHECK)"
	@which $(CPPCHECK)
	@echo
	@echo "% $(CPPCHECK) --version"
	@$(CPPCHECK) --version
	@echo
	@$(CXX) --version
	@echo "% which $(DOXYGEN)"
	@which $(DOXYGEN)
	@echo
	@echo "% $(DOXYGEN) --version"
	@$(DOXYGEN) --version
	@echo
	@echo "% which $(CXX)"
	@which $(CXX)
	@echo
	@echo "% $(CXX) --version"
	@$(CXX) --version
	@echo
	@echo "% which $(GCOV)"
	@which $(GCOV)
	@echo
	@echo "% $(GCOV) --version"
	@$(GCOV) --version
	@echo
	@echo "% which git"
	@which git
	@echo
	@echo "% git --version"
	@git --version
	@echo
	@echo "% grep \"#define BOOST_LIB_VERSION \" $(BOOST)/version.hpp"
	@grep "#define BOOST_LIB_VERSION " $(BOOST)/version.hpp
	@echo
	@echo "% cat $(GTEST)/README"
	@cat $(GTEST)/README
	@echo
	@echo "% ls -al $(LIB)/libgtest*.a"
	@ls -al $(LIB)/libgtest*.a
	@echo
	@echo "% which make"
	@which make
	@echo
	@echo "% make --version"
	@make --version
ifneq ($(shell uname -s), Life)
	@echo
	@echo "% which $(VALGRIND)"
	@which $(VALGRIND)
	@echo
	@echo "% $(VALGRIND) --version"
	@$(VALGRIND) --version
endif
	@echo "% which vim"
	@which vim
	@echo
	@echo "% vim --version"
	@vim --version
