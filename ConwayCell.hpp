#ifndef ConwayCell_h
#define ConwayCell_h

#include <iostream>

#include "AbstractCell.hpp"

class ConwayCell : public AbstractCell
{
private:
public:
    /**
     * default constructor, dead cell
     */
    ConwayCell();

    /**
     * constructor
     * @param bool alive or dead 
     */
    ConwayCell(bool alive);

    /**
     * copy constructor
     * @param ConwayCell& to copy 
     */
    ConwayCell(const ConwayCell &);

    // ---------
    // charRep
    // ---------
    /**
     * gets the char representation of this cell
     * @param int number of neighbors
     * @return char
     */
    char charRep();

    // ---------
    // updateCell
    // ---------
    /**
     * kills/ennervates a cell depending on ...
     * @param int number of neighbors
     * @return bool with whether alive
     */
    bool updateCell(int neighbors);

    // ---------
    // clone
    // ---------
    /**
     * creates clone of current cell
     * @return pointer to cell clone
     */
    ConwayCell *clone() const;

    // ---------
    // checkIfNeighbor
    // ---------
    /**
     * determine if cur cell is neighbor
     * @param int current cell row
     * @param int current cell col
     * @param int neighbor cell row
     * @param int neighbor cell col
     * @return bool if neighbor
     */
    bool checkIfNeighbor(int cRow, int cCol, int nRow, int nCol);

    /**
     * destructor for cell
     */
    ~ConwayCell(){};
};

#endif