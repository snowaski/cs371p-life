#ifndef FredkinCell_h
#define FredkinCell_h

#include <iostream>

#include "AbstractCell.hpp"

class FredkinCell : public AbstractCell
{
private:
    int _age; //age of fredkin cell

public:
    /**
     * default constructor, dead cell
     */
    FredkinCell();

    /**
     * constructor
     * @param bool alive or dead 
     */
    FredkinCell(bool alive);

    /**
     * copy constructor
     * @param FredkinCell& to copy 
     */
    FredkinCell(const FredkinCell &);

    // ---------
    // charRep
    // ---------
    /**
     * gets the char representation of this cell
     * @param int number of neighbors
     * @return char
     */
    char charRep();

    // ---------
    // updateCell
    // ---------
    /**
     * kills/ennervates a cell depending on ...
     * @param int number of neighbors
     * @return bool with whether alive
     */
    bool updateCell(int neighbors);

    // ---------
    // getAge
    // ---------
    /**
     * gets the age, specific to fredkin cells
     * @return int a
     */
    int getAge() { return _age; };

    // ---------
    // clone
    // ---------
    /**
     * creates clone of current cell
     * @return pointer to cell clone
     */
    FredkinCell *clone() const;

    // ---------
    // checkIfNeighbor
    // ---------
    /**
     * determine if cur cell is neighbor
     * @param int current cell row
     * @param int current cell col
     * @param int neighbor cell row
     * @param int neighbor cell col
     * @return bool if neighbor
     */
    bool checkIfNeighbor(int cRow, int cCol, int nRow, int nCol);

    ~FredkinCell(){};
};

#endif