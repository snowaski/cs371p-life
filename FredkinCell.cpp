#include "FredkinCell.hpp"

#include <cassert>

FredkinCell::FredkinCell(bool alive) : AbstractCell(alive)
{
    _age = 0;
}

FredkinCell::FredkinCell() : AbstractCell(false)
{
    _age = 0;
}

FredkinCell::FredkinCell(const FredkinCell &) : AbstractCell(false)
{
    _age = 0;
}

bool FredkinCell::updateCell(int neighbors)
{
    // cout << "Fredkin::update" << endl;
    // check if it should die
    if (_alive && (neighbors == 0 || neighbors == 2 || neighbors == 4))
        _alive = false;
    //only increment age if staying alive
    if (_alive)
        ++_age;
    // check if we should bring to life
    if (!_alive && (neighbors == 1 || neighbors == 3))
    {
        _alive = true;
    }
    return _alive;
}

char FredkinCell::charRep()
{
    if (_alive)
    {
        if (_age >= 10)
        {
            return '+';
        }
        else
        {
            return '0' + _age;
        }
    }
    else
    {
        return '-';
    }
}

FredkinCell *FredkinCell::clone() const
{
    return new FredkinCell(*this);
}

bool FredkinCell::checkIfNeighbor(int cRow, int cCol, int nRow, int nCol)
{
    return cRow == nRow || cCol == nCol;
}