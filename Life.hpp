#ifndef Life_h
#define Life_h

#include <vector>
#include <iostream> // ostream

using namespace std;

// ---------
// Life<T>
// ---------

template <class T>
class Life
{
private:
    vector<vector<T>> grid; //represent grid of cells
    int rows;               // # of rows in grid
    int cols;               // # of cols in grid
    int pop;                // total population of live cells

public:
    /**
     * constructor
     * @param int  rows for grid
     * @param int cols for grid
     */
    Life(int rows, int cols);

    // ---------
    // addLiveCell
    // ---------
    /**
     * set up initial live cells
     * @param int num rows
     * @param int num columns
     * @return void
     */
    void addLiveCell(int row, int col);
    // ---------
    // turn
    // ---------
    /**
     * kill/bring to life a particular cell
     * @return void
     */
    void turn();

    // ---------
    // calculateNeighbors
    // ---------
    /**
     * calculate number of neighbors per each row
     * @return vector<vector<int>> representing # neighbors
     */
    void calculateNeighbors(vector<vector<int>> &);

    // ---------
    // updateCells
    // ---------
    /**
     * update the cells for the next generation
     * @param vector<vector<int>>  # of neighbors
     * @return void
     */
    void updateCells(vector<vector<int>> &neighbors);

    // ---------
    // printGrid
    // ---------
    /**
      * print current generation
      * @param ostream  to print to
      * @param int with generation name
      * @return void
      */
    void printGrid(ostream &sout, int generation);

    // ---------
    // ~Life
    // ---------
    /**
     * destructor
     */
    ~Life();
};

#endif