#ifndef AbstractCell_h
#define AbstractCell_h

#include <vector>
#include <iostream>

using namespace std;

// ---------
// AbstractCell
// ---------
class AbstractCell
{
protected:
    bool _alive; // is cell alive or dead

    AbstractCell &operator=(const AbstractCell &) = default;

public:
    /**
     * default constructor, dead cell
     */
    AbstractCell();

    /**
     * constructor
     * @param bool alive or dead 
     */
    AbstractCell(bool);

    // ---------
    // isAlive
    // ---------
    /**
     * returns if cell alive or not
     * @return bool
     */
    const bool isAlive() const { return _alive; };

    // ---------
    // updateCell
    // ---------
    /**
     * kills/ennervates a cell depending on ...
     * @param int number of neighbors
     * @return bool status whether alive
     */
    virtual bool updateCell(int neighbors) = 0;

    // ---------
    // charRep
    // ---------
    /**
     * gets the char representation of this cell
     * @param int number of neighbors
     * @return char
     */
    virtual char charRep() = 0;

    // ---------
    // checkIfNeighbor
    // ---------
    /**
     * determine if cur cell is neighbor
     * @param int current cell row
     * @param int current cell col
     * @param int neighbor cell row
     * @param int neighbor cell col
     * @return bool if neighbor
     */
    virtual bool checkIfNeighbor(int cRow, int cCol, int nRow, int nCol) = 0;

    /**
     * destructor for cell
     */
    virtual ~AbstractCell(){};

    // ---------
    // clone
    // ---------
    /**
     * creates clone of current cell
     * @return pointer to cell clone
     */
    virtual AbstractCell *clone() const = 0;
};

#endif