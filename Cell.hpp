#ifndef Cell_h
#define Cell_h

#include "AbstractCell.hpp"

#include <iostream>

using namespace std;

class Cell
{
private:
    AbstractCell *_cell;

public:
    /**
     * default constructor, dead cell
     */
    Cell();

    /**
     * constructor
     * @param bool alive or dead 
     */
    Cell(bool);

    /**
     * copy constructor
     * @param Cell& to copy 
     */
    Cell(const Cell &);

    // ---------
    // updateCell
    // ---------
    /**
     * updates Fredkin to Conway if age == 2
     * kills/ennervates a cell depending on ...
     * @param int number of neighbors
     * @return bool status whether alive
     */
    bool updateCell(int neighbors);

    // ---------
    // isAlive
    // ---------
    /**
     * returns if cell alive or not
     * @return bool
     */
    bool isAlive() { return _cell->isAlive(); };

    // ---------
    // charRep
    // ---------
    /**
     * gets the char representation of this cell
     * @param int number of neighbors
     * @return char
     */
    char charRep();

    // ---------
    // clone
    // ---------
    /**
     * creates clone of current cell
     * @return pointer to cell clone
     */
    Cell *clone() const;

    // ---------
    // checkIfNeighbor
    // ---------
    /**
     * determine if cur cell is neighbor
     * @param int current cell row
     * @param int current cell col
     * @param int neighbor cell row
     * @param int neighbor cell col
     * @return bool if neighbor
     */
    bool checkIfNeighbor(int cRow, int cCol, int nRow, int nCol);

    /**
     * destructor for cell
     */
    ~Cell();
};

#endif