#include "Cell.hpp"
#include "FredkinCell.hpp"
#include "ConwayCell.hpp"

using namespace std;

Cell::Cell(bool alive)
{
    _cell = new FredkinCell(alive);
}

Cell::Cell()
{
    _cell = new FredkinCell(false);
}

Cell::Cell(const Cell &)
{
    _cell = new FredkinCell(false);
}

bool Cell::updateCell(int neighbors)
{
    // If we get here (the `if` succeeds) it was pointing to an object of
    // the derived class and `p` is now pointing at that derived object.
    bool alive = _cell->updateCell(neighbors);

    if (FredkinCell *const p = dynamic_cast<FredkinCell *>(_cell))
    {
        if (p->getAge() == 2)
        {
            delete _cell;
            _cell = new ConwayCell(true);
        }
    }

    return alive;
}

char Cell::charRep()
{
    return _cell->charRep();
}

Cell *Cell::clone() const
{
    return new Cell(*this);
}

bool Cell::checkIfNeighbor(int cRow, int cCol, int nRow, int nCol)
{
    return _cell->checkIfNeighbor(cRow, cCol, nRow, nCol);
}

Cell::~Cell()
{
}