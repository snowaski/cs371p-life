// ---------------
// TestDarwin.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------
#include <vector>
#include "gtest/gtest.h"

#define private public

#include "AbstractCell.hpp"
#include "ConwayCell.hpp"
#include "Life.hpp"
#include "FredkinCell.hpp"

using namespace std;

// --------
// LIFE
// --------

TEST(Life, calc_neighbor0)
{
    Life<ConwayCell> life(2, 2);

    life.addLiveCell(0, 0);
    life.addLiveCell(0, 1);
    life.addLiveCell(1, 0);
    life.addLiveCell(1, 1);

    vector<vector<int>> neighbors(2, vector<int>(2, 0));

    life.calculateNeighbors(neighbors);

    ASSERT_EQ(neighbors[0][0], 3);
}

TEST(Life, calc_neighbor1)
{
    Life<ConwayCell> life(2, 2);

    vector<vector<int>> neighbors(2, vector<int>(2, 0));

    life.calculateNeighbors(neighbors);

    ASSERT_EQ(neighbors[0][0], 0);
}

TEST(Life, add_live)
{
    Life<ConwayCell> life(2, 2);

    life.addLiveCell(0, 0);

    ASSERT_EQ(life.grid[0][0].isAlive(), true);
    ASSERT_EQ(life.grid[0][1].isAlive(), false);
}

TEST(Life, turn)
{
    Life<ConwayCell> life(2, 2);

    life.addLiveCell(0, 0);

    life.turn();

    ASSERT_EQ(life.grid[0][0].isAlive(), false);
    ASSERT_EQ(life.grid[0][1].isAlive(), false);
    ASSERT_EQ(life.grid[1][0].isAlive(), false);
    ASSERT_EQ(life.grid[1][1].isAlive(), false);
}

TEST(Life, updateCells)
{
    Life<ConwayCell> life(2, 2);

    life.addLiveCell(0, 0);

    vector<vector<int>> neighbors(2, vector<int>(2, 0));

    life.calculateNeighbors(neighbors);

    life.updateCells(neighbors);

    ASSERT_EQ(life.grid[0][0].isAlive(), false);
    ASSERT_EQ(life.grid[0][1].isAlive(), false);
    ASSERT_EQ(life.grid[1][0].isAlive(), false);
    ASSERT_EQ(life.grid[1][1].isAlive(), false);
}

// --------
// CONWAYCELL
// --------

TEST(ConwayCell, update_alive0)
{
    ConwayCell c(true);
    ASSERT_EQ(c.updateCell(2), true);
}

TEST(ConwayCell, update_alive1)
{
    ConwayCell c(false);
    ASSERT_EQ(c.updateCell(2), false);
}

TEST(ConwayCell, update_alive2)
{
    ConwayCell c(true);
    ASSERT_EQ(c.updateCell(3), true);
}

TEST(ConwayCell, update_alive3)
{
    ConwayCell c(false);
    ASSERT_EQ(c.updateCell(3), true);
}

TEST(ConwayCell, update_alive4)
{
    ConwayCell c(true);
    ASSERT_EQ(c.updateCell(4), false);
}

TEST(ConwayCell, update_alive5)
{
    ConwayCell c(true);
    ASSERT_EQ(c.updateCell(1), false);
}

// --------
// Fredkin Cell
// --------

TEST(FredkinCell, constructor0)
{
    FredkinCell f(true);

    ASSERT_EQ(f._age, 0);
    ASSERT_EQ(f.isAlive(), true);
}

TEST(FredkinCell, constructor1)
{
    FredkinCell f(false);

    ASSERT_EQ(f._age, 0);
    ASSERT_EQ(f.isAlive(), false);
}

TEST(FredkinCell, updateAlive_0)
{
    FredkinCell f(false);

    ASSERT_EQ(f.updateCell(3), true);

    ASSERT_EQ(f._age, 0);
}

TEST(FredkinCell, updateAlive_1)
{
    FredkinCell f(false);

    ASSERT_EQ(f.updateCell(1), true);

    ASSERT_EQ(f._age, 0);
}

TEST(FredkinCell, updateAlive_2)
{
    FredkinCell f(false);

    ASSERT_EQ(f.updateCell(2), false);

    ASSERT_EQ(f._age, 0);
}

TEST(FredkinCell, updateAlive_3)
{
    FredkinCell f(true);

    ASSERT_EQ(f.updateCell(0), false);

    ASSERT_EQ(f._age, 0);
}

TEST(FredkinCell, updateAlive_4)
{
    FredkinCell f(true);

    ASSERT_EQ(f.updateCell(2), false);

    ASSERT_EQ(f._age, 0);
}

TEST(FredkinCell, updateAlive_5)
{
    FredkinCell f(true);

    ASSERT_EQ(f.updateCell(4), false);

    ASSERT_EQ(f._age, 0);
}

TEST(FredkinCell, updateAlive_6)
{
    FredkinCell f(true);

    ASSERT_EQ(f.updateCell(1), true);

    ASSERT_EQ(f._age, 1);
}

TEST(FredkinCell, updateAlive_7)
{
    FredkinCell f(true);

    ASSERT_EQ(f.updateCell(1), true);
    ASSERT_EQ(f.updateCell(1), true);

    ASSERT_EQ(f._age, 2);
}
