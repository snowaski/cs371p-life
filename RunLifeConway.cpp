// --------------
// RunConway.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <sstream>  // istringstream

#include "ConwayCell.hpp"
#include "Life.hpp"

using namespace std;

// ----
// main
// ----

int main()
{
    string test_cases;
    getline(cin, test_cases);
    int num_test_cases = stoi(test_cases);

    int test = 0;
    string s;

    while (test < num_test_cases)
    {

        getline(cin, s); // blank line

        // get the dimensions of the grid
        getline(cin, s);
        istringstream dim_string(s);
        getline(dim_string, s, ' ');
        int rows = stoi(s);
        getline(dim_string, s, ' ');
        int cols = stoi(s);

        Life<ConwayCell> life(rows, cols);

        getline(cin, s);
        int num_live = stoi(s);

        // get the information of live cells
        int c = 0;
        while (c < num_live)
        {
            getline(cin, s);
            istringstream sin(s);
            getline(sin, s, ' ');
            int cell_row = stoi(s);

            getline(sin, s, ' ');
            int cell_col = stoi(s);

            life.addLiveCell(cell_row, cell_col);

            ++c;
        }

        getline(cin, s);
        istringstream settings_string(s);

        getline(settings_string, s, ' ');
        int sims = stoi(s);

        getline(settings_string, s, ' ');
        int freq = stoi(s);
        cout << "*** Life<ConwayCell> " << rows << "x" << cols << " ***\n"
             << endl;
        for (int s = 0; s <= sims; ++s)
        {
            //only print with given frequency
            if (s % freq == 0)
            {
                life.printGrid(cout, s);
                if (s != (sims / freq) * freq || test < num_test_cases - 1)
                {
                    cout << endl;
                }
            }
            //always take turn
            life.turn();
        }

        ++test;
    }
    return 0;
}
